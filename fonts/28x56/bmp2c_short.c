#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define MAX_FILE_SIZE 2048

void printbinchar(uint8_t a)
{
    int i = 0;
    for (i = 0; i < 8; i++) 
    {
        printf("%d", !!((a << i) & 0x80));
    }
}

int getpixel(unsigned char *buf, int x, int y)
{
    return (buf[buf[10] + y*4 + x/8] >> (7-(x % 8))) & 1;   
}

int main(void)
{
    FILE *f;
    unsigned char buffer[MAX_FILE_SIZE];
    int n;
    
    const char *files[] =  {
    "0_short_1bpp.bmp", 
    "1_short_1bpp.bmp",
    "2_short_1bpp.bmp",
    "3_short_1bpp.bmp",
    "4_short_1bpp.bmp",
    "5_short_1bpp.bmp",
    "6_short_1bpp.bmp",
    "7_short_1bpp.bmp",
    "8_short_1bpp.bmp",
    "9_short_1bpp.bmp",
    "-_short_1bpp.bmp"}; 

    printf("#include <stdio.h>\n\n");

    int file_num = 0;
    for(file_num = 0; file_num < 11; file_num++)
    {
        f = fopen(files[file_num], "rb");
        if (f)
        {
            n = fread(buffer, MAX_FILE_SIZE, 1, f);
        }
        else
        {
            printf("error opening file: %s\n", files[file_num]);
            return 0;
        }

/*
        printf("Preview:\n--------------------------------------\n");
        int x = 0;
        int y = 0;
        for(y = 55; y >= 0; y--)
        {
            for(x = 0; x < 28; x++)
            {
                if(getpixel(buffer, x,y))
                {
                    printf("X");
                }
                else
                {
                    printf(".");
                }
            }
            printf("\n");
        }
*/
        int page = 0;
        int col = 0;
        
        if(file_num == 10)
        {
            printf("const uint8_t dig_minus_dogs102[] = {\n", file_num);
        }
        else
        {
            printf("const uint8_t dig_%i_dogs102[] = {\n", file_num);
        }

        for(page = 0; page < 7; page++)
        {
            for(col = 0; col < 28; col++)
            {
                unsigned char byte = 0;
                int bit = 0;
                
                for(bit = 0; bit < 8; bit++)
                {
                    byte |= (getpixel(buffer, col, 55-(8*page+bit)) << (bit));
                }
                printf("0x%x,",byte);
            }
            printf("\n");
        }
        printf("};\n\n\n");
    }
} 
