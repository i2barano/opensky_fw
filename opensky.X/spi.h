#ifndef SPI_H
#define SPI_H

int16_t send_spi_8b_array(const uint8_t *data, uint16_t size);
uint8_t send_recv_spi_8b(uint8_t data);
uint16_t send_recv_spi_16b(uint16_t data);

#endif /* SPI_H */
