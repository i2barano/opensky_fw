#ifndef CONFIG_H
#define CONFIG_H

/* CONFIG1*/
#pragma config JTAGEN = OFF    /* JTAG port is disabled*/
#pragma config GCP = OFF       /* Code protection is disabled*/
#pragma config GWRP = OFF      /* Writes to program memory allowed*/
#pragma config ICS = PGx1      /* Emulator/debugger uses set 1 */
#pragma config FWDTEN = ON     /* Watchdog Timer enabled */
#pragma config WINDIS = ON     /* Watchdog Timer Window Mode disabled*/
#pragma config FWPSA = PR32    /* WDT Prescaler (1:32), 5 bit*/
#pragma config WDTPS = PS256   /* Watchdog Timer Postscaler (1:128), 256ms timeout*/

/* CONFIG2*/
#pragma config IESO = OFF       /* Int Ext Switch Over Mode disabled*/
#pragma config FNOSC = FRC      /* Primary Oscillator FRCDIV (divided by 4 for 1 MIPS)*/
#pragma config FCKSM = CSECMD   /* Clock Switching enabled, Monitor disabled*/
#pragma config OSCIOFNC = OFF   /* OSCO pin functions as port I/O (RA3)*/
#pragma config IOL1WAY = OFF    /* The IOLOCK bit can be set and cleared using the unlock sequence*/
#pragma config POSCMOD = NONE   /* Primary Oscillator disabled*/

/* CONFIG2*/
#pragma config WUTSEL= FST      /* Fast regulator start-up time used*/

#endif /* CONFIG_H */

