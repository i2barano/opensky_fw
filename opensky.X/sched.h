#ifndef SCHED_H
#define SCHED_H

#define SCH_MAX_TASKS (5)

typedef struct
{
    /* Pointer to the task (must be a 'void (void)' function)*/
    void (*p_task)(void);
    
    /* Delay (ticks) until the function will (next) be run*/
    /* - see SCH_Add_Task() for further details*/
    uint16_t delay;
    
    /* Interval (ticks) between subsequent runs.*/
    /* - see SCH_Add_Task() for further details*/
    uint16_t period;
    
    /* Incremented (by scheduler) when task is due to execute*/
    uint8_t runme;
} task_info;

uint8_t sch_add_task(void (*p_function)(), const uint16_t delay, const uint16_t period);
void sch_dispatch_tasks(void);

#endif /* SCHED_H*/
