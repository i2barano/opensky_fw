#include <p24FJ64GA002.h>
#include <stdint.h>
#include <string.h>

#include "common.h"
#include "spi.h"
#include "font_5x5.h"
#include "digit_bitmaps.h"
#include "lcd.h"

/* Full-frame framebuffer (can't read-modify-write */
uint8_t lcd_buf[LCD_W*LCD_H/8];

int16_t send_framebuffer(void)
{
    uint8_t page = 0;

    /* Assert chip select */
    LCD_CS = 0;

    /* Copy framebuffer to the 8 pages of LCD SRAM */
    for(page = 0; page < LCD_H/8; page++)
    {
        /* Command */
        LCD_CD = 0;

        /* Set/reset column address to start column of LCD (30). And set page. */
        if( send_recv_spi_8b(LCD_SET_COL_ADDR_LSB | (30 & 0x0F)) ||
            send_recv_spi_8b(LCD_SET_COL_ADDR_MSB | (30 >> 4)) ||
            send_recv_spi_8b(LCD_SET_PAGE_ADDR | page))
        {
            return 1;
        }

        /* Data */
        LCD_CD = 1;

        /* Bound-check pointer */
        if(c_assert(page*LCD_W < sizeof(lcd_buf)))
        {
            return 1;
        }

        /* Send row */
        if(send_spi_8b_array(lcd_buf + page*LCD_W, LCD_W))
        {
            return 1;
        }
    }
    LCD_CS = 1;

    return 0;
}

int16_t print_lcd_line( char *str, 
                        uint8_t length, 
                        uint8_t line, 
                        uint8_t offset, 
                        uint8_t invert)
{
    uint8_t i = 0;
    
    /* Check length of string + offset would fit, 
    check line is valid, 
    check string pointer */
    if( c_assert_code(length + offset <= MAX_CHAR_NUM_LCD, FLT_LCD_W_OVERRUN) ||
        c_assert_code(line <= 7, FLT_TOO_MANY_LCD_LINES) ||
        c_assert_code(str, FLT_LCD_STR_NULL))
    {
        return 1;
    }

    for(i = 0; i < length; i++)
    {
        /* Find pointer to the first column (8px) of character, 
        this assumes characters are 5 pixel wide with 1 pixel spacing */
        uint16_t char_offset = ((line)*LCD_W + (i + offset)*6);

        /* Reached null termination */
        if(str[i] == '\0')
        {
            break;
        }

        /* Reached last character we can display */
        if(i >= MAX_CHAR_NUM_LCD)
        {
            break;
        }
            
        /* Inversion is used for the menus, where the highlighted choice is inverted */
        if(invert)
        {
            /* Bound check, assumes 5-pixel character width */
            if(c_assert((char_offset + 5) < sizeof(lcd_buf)))
            {
                return 1;
            }

            lcd_buf[char_offset + 0] = (font[(str[i]-32)][0]);  /* Character column 0 */
            lcd_buf[char_offset + 1] = (font[(str[i]-32)][1]);  /* Character column 1 */
            lcd_buf[char_offset + 2] = (font[(str[i]-32)][2]);  /* Character column 2 */
            lcd_buf[char_offset + 3] = (font[(str[i]-32)][3]);  /* Character column 3 */
            lcd_buf[char_offset + 4] = (font[(str[i]-32)][4]);  /* Character column 4 */
            lcd_buf[char_offset + 5] = 0x00;                    /* Space between characters */
        }
        else
        {
            /* Bounds check, assumes 5-pixel character width */
            if(c_assert((char_offset + 5) < sizeof(lcd_buf)))
            {
                return 1;
            }

            lcd_buf[char_offset + 0] = ~(font[(str[i]-32)][0]); /* Character column 0 */
            lcd_buf[char_offset + 1] = ~(font[(str[i]-32)][1]); /* Character column 1 */
            lcd_buf[char_offset + 2] = ~(font[(str[i]-32)][2]); /* Character column 2 */
            lcd_buf[char_offset + 3] = ~(font[(str[i]-32)][3]); /* Character column 3 */
            lcd_buf[char_offset + 4] = ~(font[(str[i]-32)][4]); /* Character column 4 */
            lcd_buf[char_offset + 5] = 0xFF;                    /* Space between characters */
        }
    }

    return 0;
}

int16_t print_alti_digit(int16_t digit_off, int16_t value, int16_t minus)
{
    uint16_t page = 0;

    /* Accept correct digit indexes */
    /* Accept 0 to 9 digit values OR minus sign */
    if( c_assert(digit_off >= DIG_OFFSET_0 && digit_off <= DIG_OFFSET_2) ||
        c_assert((value >= 0 && value <= 9) || (minus == 1)))
    {
        return 1;
    }

    /* For each page */
    for(page = 0; page < LCD_PAGES - 1; page++)
    {
        /* Bounds check */
        if( !c_assert(page*LCD_W + digit_off + DIGIT_W) < sizeof(lcd_buf) && /* frame buffer */
            !c_assert(DIGIT_W*page + DIGIT_W <= dig_size_bytes))             /* digit bitmap */
        {
            /* Write minus digit */
            if(minus == 1)
            {
                memcpy(lcd_buf + page*LCD_W + digit_off, dig_minus_dogs102 + DIGIT_W*page, DIGIT_W);
            }
            /* Write any other digit */
            else
            {
                memcpy(lcd_buf + page*LCD_W + digit_off, dig_pointer_array[value] + DIGIT_W*page, DIGIT_W);
            }
        }
    }
 
    return 0;
}













