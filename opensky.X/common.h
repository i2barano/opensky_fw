#ifndef COMMON_H
#define	COMMON_H

#include <stdint.h>

/* RELEASE FW VERSION */
#define FW_VERSION 3

/* GPIO pin direction and data register defines */
#define LED         _LATA1
#define LED_TRIS    _TRISA1

#define LCD_CD      _LATB8
#define LCD_CD_TRIS _TRISB8

#define LCD_CS      _LATB5
#define LCD_CS_TRIS _TRISB5

#define LCD_RST     _LATB9
#define LCD_RST_TRIS _TRISB9

#define PWR_EN      _LATB11
#define PWR_EN_TRIS _TRISB11

#define VCC_M_TRIG  _LATB10
#define VCC_M_TRIG_TRIS _TRISB10

#define BUT_DOWN    _RB14
#define BUT_CENTRE  _RB13
#define BUT_UP      _RB12

/* Dynamic assert definitions */
#define c_assert(e) ((e) ? (0) : log_assert_violation(__FILE__,__LINE__,#e))
#define c_assert_code(e,c) ((e) ? (0) : log_assert_violation_code(__FILE__,__LINE__,#e,c))

/* Timeout preload values for timer2 running at 500KHz*/
#define TIMEOUT_10US    5
#define TIMEOUT_100US   50
#define TIMEOUT_1MS     500
#define TIMEOUT_10MS    5000
#define TIMEOUT_100MS   50000

/* Fault condition definitions*/
#define FLT_NONE                0
#define FLT_TIMEOUT_SPI_ARRAY   1
#define FLT_TIMEOUT_SPI_8B      2
#define FLT_TIMEOUT_SPI_16B     3
#define FLT_I2C_PREV_NOT_STOP   4
#define FLT_I2C_MSTR_EN1        5
#define FLT_I2C_PREV_NOT_START  6
#define FLT_I2C_S_TIMEOUT       7
#define FLT_I2C_P_TIMEOUT       8
#define FLT_I2C_TBF_TIMEOUT     9
#define FLT_I2C_TRSTAT_TIMEOUT  10
#define FLT_I2C_RBF_TIMEOUT     11
#define FLT_I2C_MSTR_EN2        12
#define FLT_I2C_MSTR_EN3        13
#define FLT_I2C_RSEN_TIMEOUT    14
#define FLT_I2C_RESET_INVOKED   15
#define FLT_I2C_RESET_TIMEOUT   16
#define FLT_ADC_SETUP           17
#define FLT_ALTI_SETUP          18
#define FLT_GPIO_SETUP          19
#define FLT_PPS_SETUP           20
#define FLT_SPI_SETUP           21
#define FLT_I2C_SETUP           22
#define FLT_TIMER_SETUP         23
#define FLT_LCD_SETUP           24
#define FLT_NVM_WRITE_OOR       25
#define FLT_NVM_READ_OOR        26
#define FLT_LCD_W_OVERRUN       27
#define FLT_TOO_MANY_LCD_LINES  28
#define FLT_LCD_STR_NULL        29
#define FLT_ADC_TIMEOUT         30
#define FLT_TASK_OVERRUN        31
#define MAX_FAULT_TYPES         32

/* Global device state definitions */
#define DEV_STATE_DISP_ALTI     0   /* Display altitude, this state is in lock-out (user has to press button in sequence to unlock) */
#define DEV_STATE_DISP_MENU     1   /* Display menu */

/* Settings offsets of array */
#define SETTINGS_OFF_CONTRAST   0
#define SETTINGS_OFF_SIG_FIGS   1
#define SETTINGS_OFF_DZ_OFF     2
#define SETTINGS_OFF_AUTO_ZERO  3
#define SETTINGS_OFF_CHKSUM     4
extern uint16_t settings[SETTINGS_OFF_CHKSUM + 1];
extern const uint16_t settings_default[SETTINGS_OFF_CHKSUM + 1];

void setup_timeout(uint16_t timeout_val);
inline int16_t is_timeout(void);
int16_t log_assert_violation(char *file, int32_t line, char *assert);
int16_t log_assert_violation_code(char *file, int32_t line, char *assert, uint16_t code);
void shutdown_device(void);
void erase_nvm(void);
void write_word(uint16_t word, uint16_t index);
uint16_t read_word(uint16_t index);
uint16_t crc16(uint8_t data[], uint16_t length);
int32_t delay_filter(int32_t input, uint8_t reset);
int32_t lowpass_filter(int32_t input, uint8_t reset);

extern int16_t global_state;
extern int32_t temperature_c;
extern int32_t altitude_asl_ft;
extern int32_t altitude_agl_ft;
extern int32_t altitude_dz_ft;
extern int32_t altitude_offset;
extern int32_t battery_voltage_v;
extern int8_t battery_voltage_valid;
extern int8_t temperature_valid;
extern int8_t altitude_valid;
extern uint16_t device_faulted;

extern volatile uint32_t sched_cycles;

extern uint16_t fault_occurences[MAX_FAULT_TYPES];

#endif /* COMMON_H */

