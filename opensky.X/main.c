#include <p24FJ64GA002.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>

#include "spi.h"
#include "i2c.h"
#include "common.h"
#include "menu.h"
#include "lcd.h"
#include "sched.h"
#include "digit_bitmaps.h"
#include "config.h"

/* Battery low warning threshold */
#define BAT_LOW_MV 3400

/* Altimeter register addresses*/
#define ALTI_STATUS             (uint8_t)0x00
#define ALTI_OUT_P_MSB          (uint8_t)0x01
#define ALTI_OUT_P_CSB          (uint8_t)0x02
#define ALTI_OUT_P_LSB          (uint8_t)0x03
#define ALTI_OUT_T_MSB          (uint8_t)0x04
#define ALTI_OUT_T_LSB          (uint8_t)0x05
#define ALTI_WHO_AM_I           (uint8_t)0x0C
#define ALTI_PT_DATA_CFG        (uint8_t)0x13
#define ALTI_CTRL_REG1          (uint8_t)0x26

/* Global sensor and ADC data */
int32_t temperature_c = 0;
int32_t altitude_asl_ft = -2000;
int32_t altitude_agl_ft = 0;
int32_t altitude_dz_ft = 0;
int32_t altitude_offset = 0;
int32_t battery_voltage_v = 0;

/* Valid flags */
int8_t temperature_valid = 0;
int8_t altitude_valid = 0;
int8_t battery_voltage_valid = 0;

/* Global state of device, either DEV_STATE_DISP_ALTI or DEV_STATE_DISP_MENU */
/* DISP_ALTI displays altitude, it is the critical run-mode, DEV_STATE_DISP_MENU */
/* is when user unlocked device into menu mode */
int16_t global_state = DEV_STATE_DISP_ALTI;

/* Setup internal peripherals */
static int16_t setup_gpio(void);    /* Setup output GPIOs */
static int16_t setup_pps(void);     /* Setup Peripheral Pin Select mapping */
static int16_t setup_adc(void);     /* Setup ADC for reading battery voltage */
static int16_t setup_timer4(void);  /* Setup timer 4 used for delays */
static int16_t setup_timer2(void);  /* Setup timer 2 used for timeouts */
static int16_t setup_timer1(void);  /* Setup timer 1 used for scheduler interrupts */
static int16_t setup_spi(void);     /* Setup SPI bus driver */
static int16_t setup_i2c(void);     /* Setup I2C bus driver */

/* Setup external devices */
static int16_t setup_lcd(void);

/* Delay functions, they use 16-bit Timer 2 */
static void delay_us(uint16_t);     /* Max delay: 65536us */
static void delay_ms(uint16_t);     /* Max delay: 65536ms */

/* Read battery voltage in millivolts */
static int16_t read_batt_v(int32_t *batt_v);

/* Start MPL3115A2 altimeter ADC conversion */
static void start_alti_conv(void);

/* Read MPL3115A2 conversion results (temperature and altitude) */
static int16_t read_temperature_c(int32_t *temp);
static int16_t read_altitude_ft(int32_t *temp);

static void update_display_2sigfig(void);
static void update_display_3sigfig(void);

/* Tasks ran by co-operative scheduler */
static void read_sensors(void);
static void start_alti_conv(void);
static void update_display(void);
static void switch_state_machine(void);

/* MPL3115A2 addresses */
#define ADDR_ALTI_R 0xC1 /* Altimeter read address */
#define ADDR_ALTI_W 0xC0 /* Altimeter write address */

static void load_settings(void)
{
    uint16_t i = 0;

    /* Load all the bytes in */
    for(i = 0; i < sizeof(settings)/sizeof(uint16_t); i++)
    {
        settings[i] = read_word(i);
    }

    /* Check CRC over whole settings array, if invalid, load defaults */
    if(c_assert(crc16((uint8_t*)settings,
                sizeof(settings)-sizeof(uint16_t))==settings[SETTINGS_OFF_CHKSUM]))
    {
        memcpy(settings, settings_default, sizeof(settings));
    }
}

int main(void) 
{
    /* Slightly delay boot-up.
    This is because after a shutdown, the device BORs, and then starts executing
    again when the voltage rebounds. This waits a bit while voltage drops completely
    in case of a device shutdown, otherwise device latches power on again and never
    turns off. This delay is a couple ms */
    volatile uint32_t tm = 0;
    while(tm < 10000)
    {
        tm++;
    }

    /* Make all pins digital initially */
    AD1PCFG = 0xFFFF;

    /* Latch on power */
    PWR_EN_TRIS = 0;
    PWR_EN = 0;

    /* set LED pin as output */
    LED_TRIS = 0;

    /* Load settings array from FLASH */
    load_settings();

    /* Do all setup functions*/
    c_assert_code(!setup_adc(), FLT_ADC_SETUP);
    c_assert_code(!setup_gpio(), FLT_GPIO_SETUP);
    c_assert_code(!setup_pps(), FLT_PPS_SETUP);
    c_assert_code(!setup_spi(), FLT_SPI_SETUP);
    c_assert_code(!setup_i2c(), FLT_I2C_SETUP);
    c_assert_code(!setup_timer4(), FLT_TIMER_SETUP);
    c_assert_code(!setup_timer2(), FLT_TIMER_SETUP);
    c_assert_code(!setup_timer1(), FLT_TIMER_SETUP);
    c_assert_code(!setup_lcd(), FLT_LCD_SETUP);

    /* Clear LCD*/
    memset(lcd_buf, 0xFF, sizeof(lcd_buf));
    c_assert(!send_framebuffer()); 

    /* Schedule all tasks assuming 20Hz (50ms) scheduler tick */
    sch_add_task(start_alti_conv, 0, 4);        /* 0 tick delay, 5Hz */
    sch_add_task(read_sensors, 2, 4);           /* 2 tick delay, 5Hz */
    sch_add_task(update_display, 3, 4);         /* 3 tick delay, 5Hz */
    sch_add_task(switch_state_machine, 0, 0);   /* 0 tick delay, 20Hz */
    sch_add_task(process_menu, 0, 0);           /* 0 tick delay, 20Hz */

    /* Enable Timer 1 interrupt */
    IEC0bits.T1IE = 1;

    while(1)
    {
        /* Run scheduler that runs all the tasks */
        sch_dispatch_tasks();

        /* Turn ADC off because it consumes ~0.4ma! */
        AD1CON1bits.ADON = 0x0;

        /* Turing off ADC not enough (silicon bug), need to also disable via PMD */
        PMD1bits.ADC1MD = 1;

        /* Go to idle mode, also this clears WDT */
        asm("pwrsav #0");

        /* Re-enable ADC PMD */
        PMD1bits.ADC1MD = 0;

        /* Need to re-initialize ADC from scratch due to
        previously mentioned silicon bug */
        setup_adc();
    }
    return 0;
}

static int16_t read_batt_v(int32_t *batt_v)
{
    uint16_t adc_val = 0;
    uint32_t v_mv = 0;

    /* Check for null ptr */
    if(c_assert(batt_v))
    {
        return 1;
    }
    
    setup_timeout(TIMEOUT_100US);

    /* Turn on measurement FET */
    VCC_M_TRIG = 0;
    
    AD1CHSbits.CH0SA = 9;   /* select analog input channel AN9 */
    AD1CON1bits.SAMP = 1;   /* start sampling, automatic conversion will follow */

    /* wait to complete the conversion */
    while (!AD1CON1bits.DONE && !is_timeout());

    /* Make sure no ADC timeout happened */
    if(c_assert_code(!is_timeout(), FLT_ADC_TIMEOUT))
    {
        return 1;
    }
    else
    {
        adc_val = ADC1BUF0;
    }
    
    /* Turn off measurement FET */
    VCC_M_TRIG = 1;
    
    /* Calculate voltage in mv */
    v_mv = (uint32_t)adc_val*5600/1024;

    /* Check overflow */
    if(c_assert(v_mv <= UINT16_MAX))
    {
        return 1;
    }
    
    *batt_v = v_mv;

    return 0;  
}

static int16_t setup_adc(void)
{
    AD1PCFG = 0xFFFF;           /* Initially set all analog pins as digital */
    AD1PCFGbits.PCFG9 = 0x0;    /* AN9 is used for battery measurement */
    AD1CON1bits.ADSIDL = 0x1;   /* Stop in idle */
    AD1CON1bits.FORM = 0x0;     /* 10-bit integer form */
    AD1CON1bits.SSRC = 0x7;     /* Auto-convert after sampling */
    AD1CON1bits.ASAM = 0x0;     /* Sampling begins when the SAMP bit is set */
    AD1CON2bits.VCFG = 0x0;     /* AVdd and AVss are reference voltages */
    AD1CON3bits.ADRC = 0x0;     /* Clock derived from system clock */
    AD1CON3bits.SAMC = 0x1;     /* 1 Tad Auto-Sample Time bits */
    AD1CON3bits.ADCS = 0x1;     /* A/D Conversion Clock 2*Tcy */
    AD1CON1bits.ADON = 0x1;     /* Turn A/D on */
    
    if( c_assert(AD1PCFGbits.PCFG9 == 0x0) ||
        c_assert(AD1CON1bits.ADSIDL == 0x1) ||
        c_assert(AD1CON1bits.FORM == 0x0) ||
        c_assert(AD1CON1bits.SSRC == 0x7) ||
        c_assert(AD1CON1bits.ASAM == 0x0) ||
        c_assert(AD1CON2bits.VCFG == 0x0) ||
        c_assert(AD1CON3bits.ADRC == 0x0) ||
        c_assert(AD1CON3bits.SAMC == 0x1) ||
        c_assert(AD1CON3bits.ADCS == 0x1) ||
        c_assert(AD1CON1bits.ADON == 0x1))
    {
        return 1;
    }

    return 0;
}

static int16_t setup_pps(void)
{
    /* unlock PPS */
    __builtin_write_OSCCONL(OSCCON & 0xBF);
    _IOLOCK = 0;
    
    /* Outputs */
    RPOR3bits.RP6R = 7;     /* SPI MOSI = RP6 */
    RPOR3bits.RP7R = 8;     /* SPI SCK = RP8 */
    
    /* Lock */
    __builtin_write_OSCCONL(OSCCON & 0xBF);
    _IOLOCK = 1;
    
    c_assert(RPOR3bits.RP6R == 7);
    c_assert(RPOR3bits.RP7R == 8);
 
    return 0;
}

static int16_t setup_gpio(void)
{
    /* Open drain for triggering battery voltage measurement and latching power */
    ODCBbits.ODB10 = 1;
    ODCBbits.ODB11 = 1;

    /* Set outputs */
    LED_TRIS = 0;
    LCD_CD_TRIS = 0;
    LCD_CS_TRIS = 0;
    LCD_RST_TRIS = 0;
    PWR_EN_TRIS = 0;
    VCC_M_TRIG_TRIS = 0;
    
    c_assert(ODCBbits.ODB10 == 1);
    c_assert(ODCBbits.ODB11 == 1);
    c_assert(LED_TRIS == 0);
    c_assert(LCD_CD_TRIS == 0);
    c_assert(LCD_CS_TRIS == 0);
    c_assert(LCD_RST_TRIS == 0);
    c_assert(PWR_EN_TRIS == 0);
    c_assert(VCC_M_TRIG_TRIS == 0);

    return 0;
}

static int16_t setup_timer4(void)
{
    /* We have 1MHz cycle clock with FRCDIV */
    T4CONbits.TSIDL = 1;    /* Stop in idle mode */
    T4CONbits.TCKPS = 0;    /* Divide Fcy by 1 to get 1 MHz timer tick */
    T4CONbits.TON = 1;      /* Turn peripheral on */
    PR4 = 0xFFFF;           /* Count all the way up before timer reset */

    c_assert(T4CONbits.TSIDL == 1);
    c_assert(T4CONbits.TCKPS == 0);
    c_assert(T4CONbits.TON == 1);
    c_assert(PR4 == 0xFFFF);
    
    return 0;
}

static int16_t setup_timer2(void)
{
    /* We have 1MHz cycle clock with FRCDIV */
    T2CONbits.TSIDL = 1;    /* Stop in idle mode */
    T2CONbits.TCKPS = 1;    /* Divide Fcy by 8 to get 125 KHz timer tick */
    T2CONbits.TON = 1;      /* Turn peripheral on */
    PR2 = 0xFFFF;           /* Count all the way up before timer reset */
    
    c_assert(T2CONbits.TSIDL == 1);
    c_assert(T2CONbits.TCKPS == 1);
    c_assert(T2CONbits.TON == 1);
    c_assert(PR2 == 0xFFFF);

    return 0;
}

static int16_t setup_timer1(void)
{
    /* We have 1MHz cycle clock with FRCDIV */
    T1CONbits.TSIDL = 0; /* Don't stop in idle mode, need ticks during idle still */
    T1CONbits.TCKPS = 0; /* Divide input by 1 to get full frequency */
    T1CONbits.TSYNC = 0; /* Async external clock from oscillator */
    T1CONbits.TCS = 1;   /* T1CK clock input from oscillator */
    T1CONbits.TON = 1;   /* Turn peripheral on */
    PR1 = 1638;          /* 50ms (20Hz) period/ISR invocation */

    c_assert(T1CONbits.TSIDL == 0);
    c_assert(T1CONbits.TCKPS == 0);
    c_assert(T1CONbits.TON == 1);
    c_assert(PR1 == 1638);

    /* Highest interrupt priority, this shall be the ONLY interrupt in system */
    IPC0bits.T1IP = 7;

    c_assert(IPC0bits.T1IP == 7);
    
    return 0;
}

static void delay_ms(uint16_t ms)
{
    uint16_t i = 0;
    for(i = 0; i < ms; i++)
    {
        delay_us(1000); /* Will delay slightly more than 1ms due to instructions */
    }
}
static void delay_us(uint16_t us)
{
    PR4 = us;               /* 1 MHz timer tick, so 1 tick is 1uS */
    IFS1bits.T4IF = 0;
    TMR4 = 0;               /* Reset timer counter */
    while(!IFS1bits.T4IF);
}

static int16_t setup_spi(void)
{
    
    SPI1STATbits.SPISIDL = 1;   /* Stop in idle */
    SPI1CON1bits.SMP = 0;       /* Sample in middle */
    SPI1CON1bits.CKE = 0;       /* SPI Mode 3 */
    SPI1CON1bits.CKP = 1;       /* SPI mode 3 */
    SPI1CON1bits.MSTEN = 1;     /* Master mode */
    SPI1CON1bits.SPRE = 7;      /* Secondary prescale 1:1 */
    SPI1CON1bits.PPRE = 3;      /* Primary prescale 1:1 */
    SPI1CON2bits.SPIBEN = 0;    /* Enhanced mode initially disabled */
    SPI1STATbits.SPIEN = 1;
    
    c_assert(SPI1STATbits.SPIEN == 1);
    c_assert(SPI1STATbits.SPISIDL == 1);   /* Stop in idle */
    c_assert(SPI1CON1bits.SMP == 0);       /* Sample in middle */
    c_assert(SPI1CON1bits.CKE == 0);       /* SPI Mode 3 */
    c_assert(SPI1CON1bits.CKP == 1);       /* SPI mode 3*/
    c_assert(SPI1CON1bits.MSTEN == 1);     /* Master mode */
    c_assert(SPI1CON1bits.SPRE == 7);      /* Secondary prescale 1:1 */
    c_assert(SPI1CON1bits.PPRE == 3);      /* Primary prescale 1:1 */
    
    return 0;
}

static int16_t setup_i2c(void)
{
    I2C2BRG = 9;             /* 125 KHz SCL frequency measured (with 1 MHz Fcy) */
    I2C2CONbits.I2CSIDL = 1; /* Stop in idle mode */
    I2C2CONbits.DISSLW = 1;  /* Disable slew control (oscillates otherwise) */
    I2C2CONbits.I2CEN = 1;   /* Enable I2C */

    c_assert(I2C2BRG == 9);
    c_assert(I2C2CONbits.I2CSIDL == 1);
    c_assert(I2C2CONbits.DISSLW == 1);   
    c_assert(I2C2CONbits.I2CEN == 1);

    return 0;
}

static int16_t setup_lcd(void)
{
    /* Set CS and CD to known values */
    LCD_CS = 1;
    LCD_CD = 1;
    
    /* Pulse reset line low to reset LCD */
    LCD_RST = 0;
    delay_ms(10u);
    LCD_RST = 1;
    delay_ms(10u);

    /* Sending commands */
    LCD_CD = 0;

    /* Chip select */
    LCD_CS = 0;

    /* Let CS and CD signals stabilize */
    delay_ms(1u);

    if(send_recv_spi_8b(LCD_SET_PWR_CTRL | 0x07) ||      /* All regulation ON */
       send_recv_spi_8b(LCD_SET_SCRL_LINE | 0x00) ||     /* Scroll-line at 0 */
       send_recv_spi_8b(LCD_CONF_VLCD_RES | 0x07) ||     /* Set VLCD R Ratio */
       send_recv_spi_16b(LCD_SET_CNTRST_16b | 0x0026) || /* Set electronic volume */
       send_recv_spi_8b(LCD_SET_BIAS | 0x0) ||           /* Set bias ratio to 1/7 */
       send_recv_spi_8b(LCD_SEG_DIR | 0x00) ||           /* Normal SEG direction */
       send_recv_spi_8b(LCD_COM_DIR | 0x08) ||           /* Mirrored COM direction*/
       send_recv_spi_8b(LCD_DISP_EN | 0x01) ||           /* Enable display */
       send_recv_spi_8b(LCD_SET_INV_DISP | 1))           /* Inverse display */
    {
        return 1;
    }
    
    /* De-select LCD */
    LCD_CS = 1;

    return 0;
}

static int16_t read_temperature_c(int32_t *temp)
{
    uint8_t status_reg = 0;
    uint8_t t_msb = 0;
    uint8_t t_lsb = 0;

    if( c_assert(temp))
    {   
        return 1;
    }

    setup_timeout(TIMEOUT_1MS);

    /* Wait for temperature data available */
    while(!is_timeout())
    {
        if( i2c_start(ADDR_ALTI_W) ||
            i2c_send(ALTI_STATUS) ||
            i2c_rep_start(ADDR_ALTI_R) ||
            i2c_recv_nack(&status_reg))
        {
            i2c_reset();
            return 1;
        }
        else
        {
            i2c_stop();
        }

        /* Check status reg for temperature data ready bit */
        if(status_reg & (1 << 1))
        {
            break;
        }
    }

    /* Make sure no timeout happened */
    if(c_assert(!is_timeout()))
    {
        return 1;
    }

    /* Read temperature MSB and LSB */
    if( i2c_start(ADDR_ALTI_W) ||
        i2c_send(ALTI_OUT_T_MSB) ||
        i2c_rep_start(ADDR_ALTI_R) ||
        i2c_recv_ack(&t_msb) ||
        i2c_recv_nack(&t_lsb))
    {
        i2c_reset();
        return 1;
    }
    else
    {
        i2c_stop();
    }

    /* Calculate temp in C, cast to int8 first to enable sign extension */
    *temp = (int32_t)(int8_t)(t_msb);

    return 0;
}

static int16_t read_altitude_ft(int32_t *alti)
{
    uint8_t status_reg = 0;
    uint8_t p_msb = 0;
    uint8_t p_csb = 0;
    uint8_t p_lsb = 0;

    if( c_assert(alti))
    {   
        return 1;
    }

    setup_timeout(TIMEOUT_1MS);

    /* Wait for altitude data available */
    while(!is_timeout())
    {
        if( i2c_start(ADDR_ALTI_W) ||
            i2c_send(ALTI_STATUS) ||
            i2c_rep_start(ADDR_ALTI_R) ||
            i2c_recv_nack(&status_reg))
        {
            i2c_reset();
            return 1;
        }
        else
        {
            i2c_stop();
        }

        /* Check Pressure/Altitude data ready bit */
        if(status_reg & (1 << 2))
        {
            break;
        }
    }

    /* Check that no timeout happened */
    if(c_assert(!is_timeout()))
    {
        return 1;
    }

    /* Read altitude MSB, CSB, and LSB */
    if( i2c_start(ADDR_ALTI_W) ||
        i2c_send(ALTI_OUT_P_MSB) ||
        i2c_rep_start(ADDR_ALTI_R) ||
        i2c_recv_ack(&p_msb) ||
        i2c_recv_ack(&p_csb) ||
        i2c_recv_nack(&p_lsb))
    {
        i2c_reset();   
        return 1;
    }
    else
    {
        i2c_stop();
    }

    /* Assemble altitude from the MSB, CSB, LSB components */
    *alti = ((int32_t)( (uint32_t)p_msb << 24 |
                        (uint32_t)p_csb << 16 |
                        (uint32_t)p_lsb << 8))/19975;

    return 0;
}

static void read_sensors(void)
{
    static int32_t below_100_count = 0;
    static int32_t initial = 1;

    /* Reset all valid flags */
    battery_voltage_valid = 0;
    altitude_valid = 0;
    temperature_valid = 0;

    if(!read_batt_v(&battery_voltage_v))
    {
        battery_voltage_valid = 1;
    }
    else /* Battery voltage not strictly critical, keep going */
    {
    }

    if(!read_temperature_c(&temperature_c))
    {
        temperature_valid = 1;
    }
    else /* Temperature not strictly critical, keep going */
    {
    }

    if(!read_altitude_ft(&altitude_asl_ft))
    {
        altitude_valid = 1;
    }
    else
    {
        /* Altitude is critical, return without doing math or zeroing anything out*/
        return;
    }

    /* Reset altimeter after every read so it can't get into a bad state */
    if( i2c_start(ADDR_ALTI_W) ||
        i2c_send(ALTI_PT_DATA_CFG) ||
        i2c_send(1 << 2))
    {
        i2c_reset();
    }
    else
    {
        i2c_stop();
    }

    /* Zero altitude on boot */
    if(initial)
    {
        altitude_dz_ft = altitude_asl_ft;
        initial = 0;
    }

    /* Perform the dz offset */
    altitude_agl_ft = altitude_asl_ft - altitude_dz_ft;

    /* Update dz elevation */
    if( (below_100_count >= 2000) &&    /* If we've been at [-100, 100] for 480s+ */
        (settings[SETTINGS_OFF_AUTO_ZERO] == 1u)) /* If autozero is enabled */
    {
        altitude_dz_ft = lowpass_filter(delay_filter(altitude_asl_ft, 0), 0);
    }

    /* Update how long we've been below 100ft AND above -100ft */
    if((altitude_agl_ft < 100) && (altitude_agl_ft > -100))
    {
        /* int_32 will overflow in 17 years runtime */
        below_100_count++;
    }
    else
    {
        below_100_count = 0;
    }

    /* Perform DZ offset */
    altitude_agl_ft = altitude_agl_ft - altitude_offset;
}

static void start_alti_conv(void)
{
    /* Enable data ready flags */
    if( i2c_start(ADDR_ALTI_W) ||
        i2c_send(ALTI_PT_DATA_CFG) ||
        i2c_send(7))
    {
        /* Fail */
        i2c_reset();
        return;
    }
    else
    {
        i2c_stop();
    }

    /* Initiate measurement immediately,
    altimeter mode,
    oversample = 16 (66ms between samples) */
    if( i2c_start(ADDR_ALTI_W) ||
        i2c_send(ALTI_CTRL_REG1) ||
        i2c_send((1 << 7)|(1 << 1)|(1 << 5)))
    {
        /* Fail */
        i2c_reset();
    }
    else
    {
        i2c_stop();
    }

    /* No fail */
}

static void update_display(void)
{
    if(settings[SETTINGS_OFF_SIG_FIGS] == 3)
    {
        update_display_3sigfig();
    }
    else if(settings[SETTINGS_OFF_SIG_FIGS] == 2)
    {
        update_display_2sigfig();
    }
    else
    {
        /* Wrong settings */

        /* Clear display */
        memset(lcd_buf, 0xFF, sizeof(lcd_buf));
        send_framebuffer();
        LED = 1;
    }
}

static void update_display_2sigfig(void)
{
    /* Get digit numbers from altitude, absolute since sign handled separately */
    int8_t dig_0_val = abs(altitude_agl_ft/10000l % 10);
    int8_t dig_1_val = abs(altitude_agl_ft/1000l % 10);
    int8_t dig_2_val = abs(altitude_agl_ft/100l % 10);
    int8_t dig_3_val = abs(altitude_agl_ft/10l % 10);

    uint16_t i = 0;
    uint8_t page = 0; /* LCD memory and framebuffer Page counter used in for-loops*/

    /* Buffer and it's length for end of screen agl and dz altitudes,
    also for "too low/high" messages when out of bounds */
    char str[26];
    uint8_t len = 0;

    /* If we're in any other state than display altitude, don't do anything */
    if(global_state != DEV_STATE_DISP_ALTI)
    {
        return;
    }

    /* Make sure all are within range */
    c_assert((dig_0_val >= 0) && (dig_0_val < 10));
    c_assert((dig_1_val >= 0) && (dig_1_val < 10));
    c_assert((dig_2_val >= 0) && (dig_2_val < 10));
    c_assert((dig_3_val >= 0) && (dig_2_val < 10));

    /* First clear everything */
    memset(lcd_buf, 0xFF, sizeof(lcd_buf));

    /* Put dashes everywhere, format: -.-- */
    if(!altitude_valid)
    {
        print_alti_digit(DIG_OFFSET_0, 0, 1);
        print_alti_digit(DIG_OFFSET_1_L, 0, 1);
        print_alti_digit(DIG_OFFSET_2, 0, 1);
    }
    /* Below displayable altitude (<= -10000) */
    else if(altitude_agl_ft <= -10000)
    {
        len = snprintf(str, sizeof(str), "AGL TOO LOW");
        print_lcd_line(str, len, page, 0, 0);
    }
    /* [-9999;-100], format: -1.0 */
    else if((altitude_agl_ft >= -9999) && (altitude_agl_ft <= -10))
    {
        print_alti_digit(DIG_OFFSET_0, 0, 1);
        print_alti_digit(DIG_OFFSET_1_H, dig_1_val, 0);
        print_alti_digit(DIG_OFFSET_2, dig_2_val, 0);
    }
    /* [-9;9999], format: 1.0 */
    else if((altitude_agl_ft >= -9) && (altitude_agl_ft <= 9999))
    {
        print_alti_digit(DIG_OFFSET_1_H, dig_1_val, 0);
        print_alti_digit(DIG_OFFSET_2, dig_2_val, 0);
    }
    /* [10000;99999],   format: 10.0 */
    else if((altitude_agl_ft >= 10000) && (altitude_agl_ft <= 99999))
    {
        print_alti_digit(DIG_OFFSET_0, dig_0_val, 0);
        print_alti_digit(DIG_OFFSET_1_H, dig_1_val, 0);
        print_alti_digit(DIG_OFFSET_2, dig_2_val, 0);
    }
    /* Above displayable altitude (>=100000) */
    else if(altitude_agl_ft >= 100000)
    {
        len = snprintf(str, sizeof(str), "AGL TOO HIGH");
        print_lcd_line(str, len, page, 0, 0);
    }
    else
    {
    }

    /* Print raw altitudes on bottom line */
    if(altitude_valid)
    {
        len = snprintf( str,
                        sizeof(str),
                        "AG%+06li DZ%+06li",
                        altitude_agl_ft,
                        altitude_dz_ft);
    }
    else
    {
        len = snprintf(str, sizeof(str), "AG?????? DZ%+06li", altitude_dz_ft);
    }
    print_lcd_line(str, len, 7, 0, 0);

    /* Add dot if at [-9999;100000] */
    if((altitude_agl_ft >= -9999) && (altitude_agl_ft <= 100000))
    {
        /* Add square dot between digits */
        for(i = 63; i <= 70; i++)
        {
            /* Bound check */
            if(c_assert(i + 6*LCD_W < sizeof(lcd_buf)))
            {
                return;
            }

            lcd_buf[i + 6*LCD_W] = 0x00;
        }
    }

    c_assert(!send_framebuffer());
}

static void update_display_3sigfig(void)
{
    /* Get digit numbers from altitude, absolute since sign handled separately*/
    int8_t dig_0_val = abs(altitude_agl_ft/10000l % 10);
    int8_t dig_1_val = abs(altitude_agl_ft/1000l % 10);
    int8_t dig_2_val = abs(altitude_agl_ft/100l % 10);
    int8_t dig_3_val = abs(altitude_agl_ft/10l % 10);

    uint16_t i = 0;
    uint8_t page = 0; /* LCD memory and framebuffer Page counter used in for-loops*/

    /* Buffer and it's length for end of screen agl and dz altitudes,
    also for "too low/high" messages when out of bounds */
    char str[26];
    uint8_t len = 0;

    /* If we're in any other state than display altitude, don't do anything */
    if(global_state != DEV_STATE_DISP_ALTI)
    {
        return;
    }

    /* Make sure all are within range */
    c_assert((dig_0_val >= 0) && (dig_0_val < 10));
    c_assert((dig_1_val >= 0) && (dig_1_val < 10));
    c_assert((dig_2_val >= 0) && (dig_2_val < 10));
    c_assert((dig_3_val >= 0) && (dig_2_val < 10));

    /* First clear everything */
    memset(lcd_buf, 0xFF, sizeof(lcd_buf));

    /* Put dashes everywhere, format: -.-- */
    if(!altitude_valid)
    {
        print_alti_digit(DIG_OFFSET_0, 0, 1);
        print_alti_digit(DIG_OFFSET_1_L, 0, 1);
        print_alti_digit(DIG_OFFSET_2, 0, 1);
    }
    /* Below displayable altitude (<= -10000) */
    else if(altitude_agl_ft <= -10000)
    {
        len = snprintf(str, sizeof(str), "AGL TOO LOW");
        print_lcd_line(str, len, page, 0, 0);
    }
    /* [-9999;-1000], format: -1.0 */
    else if((altitude_agl_ft >= -9999) && (altitude_agl_ft <= -1000))
    {
        print_alti_digit(DIG_OFFSET_0, 0, 1);
        print_alti_digit(DIG_OFFSET_1_H, dig_1_val, 0);
        print_alti_digit(DIG_OFFSET_2, dig_2_val, 0);
    }
    /* [-999;-10], format: -.00 */
    else if((altitude_agl_ft >= -999) && (altitude_agl_ft <= -10))
    {
        print_alti_digit(DIG_OFFSET_0, 0, 1);
        print_alti_digit(DIG_OFFSET_1_L, dig_2_val, 0);
        print_alti_digit(DIG_OFFSET_2, dig_3_val, 0);
    }
    /* [-9;9999], format: 1.00 */
    else if((altitude_agl_ft >= -9) && (altitude_agl_ft <= 9999))
    {
        print_alti_digit(DIG_OFFSET_0, dig_1_val, 0);
        print_alti_digit(DIG_OFFSET_1_L, dig_2_val, 0);
        print_alti_digit(DIG_OFFSET_2, dig_3_val, 0);
    }
    /* [10000;99999], format: 10.0 */
    else if((altitude_agl_ft >= 10000) && (altitude_agl_ft <= 99999))
    {
        print_alti_digit(DIG_OFFSET_0, dig_0_val, 0);
        print_alti_digit(DIG_OFFSET_1_H, dig_1_val, 0);
        print_alti_digit(DIG_OFFSET_2, dig_2_val, 0);
    }
    /* Above displayable altitude (>=100000) */
    else if(altitude_agl_ft >= 100000)
    {
        len = snprintf(str, sizeof(str), "AGL TOO HIGH");
        print_lcd_line(str, len, page, 0, 0);
    }
    else
    {
    }

    /* Print raw altitudes on bottom line */
    if(altitude_valid)
    {
        len = snprintf( str,
                        sizeof(str),
                        "AG%+06li DZ%+06li",
                        altitude_agl_ft,
                        altitude_dz_ft);
    }
    else
    {
        len = snprintf(str, sizeof(str), "AG?????? DZ%+06li", altitude_dz_ft);
    }
    print_lcd_line(str, len, 7, 0, 0);

    /* Add square dot between digits */
    /* [-9999;-1000], format: -1.0 */
    /* [10000;99999], format: 10.0 */
    if(((altitude_agl_ft >= -9999) && (altitude_agl_ft <= -1000)) ||
       ((altitude_agl_ft >= 10000) && (altitude_agl_ft <= 99999)))
    {
        /* Them pixels */
        for(i = 63; i <= 70; i++)
        {
            /* Bound check */
            if(c_assert(i + 6*LCD_W < sizeof(lcd_buf)))
            {
                return;
            }

            lcd_buf[i + 6*LCD_W] = 0x00;
        }
    }
    /* [-999;-10], format: -.00 */
    /* [-9;9999], format: 1.00 */
    /* Altitude invalid */
    else if(((altitude_agl_ft >= -999) && (altitude_agl_ft <= -10)) ||
            ((altitude_agl_ft >= -9) && (altitude_agl_ft <= 9999)) ||
            (!altitude_valid))
    {
        /* Them pixels */
        for(i = 31; i <= 38; i++)
        {
            /* Bound check*/
            if(c_assert(i + 6*LCD_W < sizeof(lcd_buf)))
            {
                return;
            }

            lcd_buf[i + 6*LCD_W] = 0x00;
        }
    }
    else
    {
    }
    
    c_assert(!send_framebuffer());
}

/* AAD-style switch state machine, assumes it is called at 20Hz, handles LED */
void switch_state_machine(void)
{
    static uint32_t shutoff_counter = 0;
    static uint8_t button_state = 0;
    static uint8_t but_counter = 0;

    static uint8_t but_centre_prev = 0;

    /* Read the button */
    uint8_t but_centre_curr = BUT_CENTRE;

    /* Increment shutoff counter when no button is pressed AND
    when agl less than 2k */
    if(but_centre_curr && (altitude_agl_ft < 2000))
    {
        /* Increment shutoff counter to keep track of when to -shutoff altimeter */
        shutoff_counter++;
    }
    else
    {
        /* reset whenever button is pressed, or when we are above 2k ft */
        shutoff_counter = 0;
    }

    /* Check if we exceeded our shutoff time of 14 hours */
    if(shutoff_counter >= 20ul*60ul*60ul*14ul)
    {
        shutdown_device();
    }

    /* If we're not in the altitude display state, don't do the state machine */
    if(global_state != DEV_STATE_DISP_ALTI)
    {
        return;
    }

    /* Increment counter to keep track of time */
    but_counter++;

    /* AAD-style state machine */
    switch(button_state)
    {
        case 0:
            /* Solid LED ON on fault */
            if(device_faulted)
            {
                LED = 1;
            }
            /* Flash LED on low battery or invalid battery reading */
            else if(but_counter > 50 &&
                    (((battery_voltage_v < BAT_LOW_MV) && battery_voltage_valid) ||
                    !battery_voltage_valid))
            {
                LED = 1;
                but_counter = 0;
            }
            else
            {
                LED = 0;
            }

            if(but_centre_prev && !but_centre_curr)
            {
                button_state = 1;
                but_counter = 0;
            }
            break;
        case 1:
            if(but_counter < 24)
            {
                if(but_centre_prev && !but_centre_curr)
                {
                    button_state = 0;
                }
                LED = 0;
            }
            else
            {
                button_state = 2;
                LED = 1;
            }
            break;
        case 2:
            if(((but_counter >= 24) && (but_counter < 40)))
            {
                if(but_centre_prev && !but_centre_curr)
                {
                    button_state = 3;
                    LED = 0;
                    but_counter = 0;
                }
            }
            else
            {
                button_state = 0;
                LED = 0;
            }
            break;
        case 3:
            if(but_counter < 24)
            {
                if(but_centre_prev && !but_centre_curr)
                {
                    button_state = 0;
                }
                LED = 0;
            }
            else
            {
                button_state = 4;
                LED = 1;
            }
            break;
         case 4:
            if(((but_counter >= 24) && (but_counter < 40)))
            {
                if(but_centre_prev && !but_centre_curr)
                {
                    LED = 0;

                    /* Change global state to display menu */
                    global_state = DEV_STATE_DISP_MENU;

                    button_state = 0;
                    LED = 0;
                }
            }
            else
            {
                button_state = 0;
                LED = 0;
            }
            break;
    }

    /* Store the previous value */
    but_centre_prev = but_centre_curr;
}
