#ifndef DIGIT_BITMAPS_H
#define DIGIT_BITMAPS_H

#define DIGIT_W 28

#include <stdint.h>

extern const uint8_t dig_0_dogs102[];
extern const uint8_t dig_1_dogs102[];
extern const uint8_t dig_2_dogs102[];
extern const uint8_t dig_3_dogs102[];
extern const uint8_t dig_4_dogs102[];
extern const uint8_t dig_5_dogs102[];
extern const uint8_t dig_6_dogs102[];
extern const uint8_t dig_7_dogs102[];
extern const uint8_t dig_8_dogs102[];
extern const uint8_t dig_9_dogs102[];
extern const uint8_t dig_minus_dogs102[];
extern const uint8_t *dig_pointer_array[];
extern const uint16_t dig_size_bytes;

#endif /* DIGIT_BITMAPS_H */
