#include <stdint.h>
#include <p24FJ64GA002.h>

#include "common.h"
#include "sched.h"

volatile uint32_t sched_cycles = 0;                 /* Scheduler cycle count */

static volatile task_info sch_tasks[SCH_MAX_TASKS]; /* Task list, modified in ISR */
static volatile int16_t task_overrun = 0;           /* Task overrun flag, set in ISR */
static int16_t dispatch_active = 0;                 /* Flag set by dispatch functions, used to detect overruns in ISR */

/* Schedule task for execution */
uint8_t sch_add_task(void (*p_function)(), const uint16_t delay, const uint16_t period)
{
    uint16_t index = 0;

    /* First find a gap in the array (if there is one)*/
    while ((sch_tasks[index].p_task != 0) && (index < SCH_MAX_TASKS))
    {
        index++;
    }

    /* Have we reached the end of the list?*/
    if (index == SCH_MAX_TASKS)
    {
        return 1;
    }

    /* If we're here, there is a space in the task array*/
    sch_tasks[index].p_task = p_function;
    sch_tasks[index].delay = delay;
    sch_tasks[index].period = period;
    sch_tasks[index].runme = 0;

    return 0;
}

/* Timer ISR (one ISR in whole application) invoked every scheduler tick */
void __attribute__((interrupt(auto_psv))) _T1Interrupt(void) 
{
    uint8_t index = 0;
    IFS0bits.T1IF = 0;

    /* Check if task is overrun */
    if(dispatch_active)
    {
        task_overrun = 1;
    }

    sched_cycles++;

    /* NOTE: calculations are in *TICKS* (not milliseconds) */
    for (index = 0; index < SCH_MAX_TASKS; index++)
    {
        /* Check if there is a task at this location */
        if (sch_tasks[index].p_task)
        {
            if (sch_tasks[index].delay == 0)
            {
                /* The task is due to run */
                sch_tasks[index].runme += 1; /* Inc. the 'runme' flag */
                if (sch_tasks[index].period)
                {
                    /* Schedule periodic tasks to run again */
                    sch_tasks[index].delay = sch_tasks[index].period;
                }
            }
            else
            {
                /* Not yet ready to run: just decrement the delay */
                sch_tasks[index].delay -= 1;
            }
        }
    }
}

void sch_dispatch_tasks(void)
{
    uint8_t index;

    dispatch_active = 1;
    
    /* Dispatches (runs) the next task (if one is ready) */
    for (index = 0; index < SCH_MAX_TASKS; index++)
    {
        if (sch_tasks[index].runme > 0)
        {
            (*sch_tasks[index].p_task)(); /* Run the task */
            sch_tasks[index].runme -= 1; /* Reset / reduce RunMe flag */
        }
    }

    /* Log and reset task overrun flag */
    if(c_assert_code(!task_overrun, FLT_TASK_OVERRUN))
    {
        task_overrun = 0;
    }

    dispatch_active = 0;
}










