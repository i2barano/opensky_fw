#include <p24FJ64GA002.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "common.h"
#include "lcd.h"
#include "spi.h"
#include "menu.h"

/* Struct for state transitions */
typedef struct
{
    uint8_t state;
    uint8_t input;
    uint8_t nextstate;
} MENU_NEXTSTATE;

/* Struct for menu entry definitions */
typedef struct
{
    uint8_t state;
    char *p_text;
    void (*p_func)(uint8_t input);
} MENU_STATE;

/* State list */
#define ST_ENTRY1   1
#define ST_ENTRY2   2
#define ST_ENTRY3   3
#define ST_ENTRY4   4
#define ST_ENTRY5   5
#define ST_ENTRY6   6
#define ST_ENTRY7   7
#define ST_ENTRY8   8
#define ST_SHUTDOWN 9
#define ST_ALTI     10
#define ST_STATUS   11
#define ST_ALTITUDE_OFFSET 12
#define ST_ZERO_ALTITUDE 13
#define ST_ABOUT    14
#define ST_SIG_FIGS 15
#define ST_AUTO_ZERO 16

/* Input list */
#define KEY_NONE    0
#define KEY_UP      1
#define KEY_DOWN    2
#define KEY_CENTRE  3

MENU_NEXTSTATE menu_nextstate[] = {
/*  STATE                       INPUT       NEXT STATE */
    {ST_ENTRY1,                 KEY_UP,     ST_ENTRY8},
    {ST_ENTRY1,                 KEY_DOWN,   ST_ENTRY2},
    {ST_ENTRY1,                 KEY_CENTRE, ST_ALTI},

    {ST_ENTRY2,                 KEY_UP,     ST_ENTRY1},
    {ST_ENTRY2,                 KEY_DOWN,   ST_ENTRY3},
    {ST_ENTRY2,                 KEY_CENTRE, ST_SHUTDOWN},

    {ST_ENTRY3,                 KEY_UP,     ST_ENTRY2},
    {ST_ENTRY3,                 KEY_DOWN,   ST_ENTRY4},
    {ST_ENTRY3,                 KEY_CENTRE, ST_STATUS},

    {ST_ENTRY4,                 KEY_UP,     ST_ENTRY3},
    {ST_ENTRY4,                 KEY_DOWN,   ST_ENTRY5},
    {ST_ENTRY4,                 KEY_CENTRE, ST_ALTITUDE_OFFSET},

    {ST_ENTRY5,                 KEY_UP,     ST_ENTRY4},
    {ST_ENTRY5,                 KEY_DOWN,   ST_ENTRY6},
    {ST_ENTRY5,                 KEY_CENTRE, ST_ZERO_ALTITUDE},

    {ST_ENTRY6,                 KEY_UP,     ST_ENTRY5},
    {ST_ENTRY6,                 KEY_DOWN,   ST_ENTRY7},
    {ST_ENTRY6,                 KEY_CENTRE, ST_ABOUT},

    {ST_ENTRY7,                 KEY_UP,     ST_ENTRY6},
    {ST_ENTRY7,                 KEY_DOWN,   ST_ENTRY8},
    {ST_ENTRY7,                 KEY_CENTRE, ST_SIG_FIGS},

    {ST_ENTRY8,                 KEY_UP,     ST_ENTRY7},
    {ST_ENTRY8,                 KEY_DOWN,   ST_ENTRY1},
    {ST_ENTRY8,                 KEY_CENTRE, ST_AUTO_ZERO},

    {ST_STATUS,                 KEY_CENTRE, ST_ENTRY3},
    {ST_ZERO_ALTITUDE,          KEY_NONE,   ST_ENTRY5},     /* Transient */
    {ST_ABOUT,                  KEY_CENTRE, ST_ENTRY6},
};

void menu_level_1(uint8_t input);
void menu_set_alti_state(uint8_t input);
void menu_show_status(uint8_t input);
void menu_altitude_offset(uint8_t input);
void menu_contrast(uint8_t input);
void menu_zero_altitude(uint8_t input);
void menu_about(uint8_t input);
void menu_sig_figs(uint8_t input);
void menu_auto_zero(uint8_t input);
void menu_shutdown(uint8_t input);

MENU_STATE menu_state[] = {
/*  STATE           STATE TEXT             STATE_FUNC */
    {ST_ENTRY1,     " BACK TO ALTI    ",   menu_level_1},
    {ST_ENTRY2,     " SHUTDOWN        ",   menu_level_1},
    {ST_ENTRY3,     " STATUS          ",   menu_level_1},
    {ST_ENTRY4,     " ALTITUDE OFFSET ",   menu_level_1},
    {ST_ENTRY5,     " ZERO ALTITUDE   ",   menu_level_1},
    {ST_ENTRY6,     " ABOUT           ",   menu_level_1},
    {ST_ENTRY7,     " SIGNIFICANT FIGS",   menu_level_1},
    {ST_ENTRY8,     " AUTO ZERO EN/DIS",   menu_level_1},
    {ST_SHUTDOWN,   0,                     menu_shutdown},
    {ST_ALTI,       0,                     menu_set_alti_state},
    {ST_STATUS,     0,                     menu_show_status},
    {ST_ALTITUDE_OFFSET, 0,                menu_altitude_offset},
    {ST_ZERO_ALTITUDE,0,                   menu_zero_altitude},
    {ST_ABOUT,      0,                     menu_about},
    {ST_SIG_FIGS,   0,                     menu_sig_figs},
    {ST_AUTO_ZERO,  0,                     menu_auto_zero},
};

const uint8_t MENU_NEXTSTATE_SIZE = sizeof(menu_nextstate)/sizeof(menu_nextstate[0]);
const uint8_t MENU_STATE_SIZE = sizeof(menu_state)/sizeof(menu_state[0]);

uint8_t current_state = ST_ENTRY1;

void process_menu(void)
{
    static uint8_t but_centre_prev = 0;
    static uint8_t but_up_prev = 0;
    static uint8_t but_down_prev = 0;

    static uint8_t input_prev = KEY_NONE;
    
    uint8_t but_centre_curr = BUT_CENTRE;
    uint8_t but_up_curr = BUT_UP;
    uint8_t but_down_curr = BUT_DOWN;

    uint8_t input = KEY_NONE;
    uint8_t i = 0;
    uint8_t k = 0;

    if(global_state != DEV_STATE_DISP_MENU)
    {
        return;
    }

    /* Detect falling edges of buttons */
    if(!but_centre_curr && but_centre_prev)
    {
        input = KEY_CENTRE;
    }
    else if(!but_up_curr && but_up_prev)
    {
        input = KEY_UP;   
    }
    else if(!but_down_curr && but_down_prev)
    {
        input = KEY_DOWN;
    }
    else
    {
        input = KEY_NONE;
    }

    if(input_prev == KEY_CENTRE && input == KEY_CENTRE)
    {
        LED = 1;
    }
    
    input_prev = input;
    
    but_up_prev = but_up_curr;
    but_down_prev = but_down_curr;
    but_centre_prev = but_centre_curr;

    /* Update to new state if necessary */
    for(i = 0; i < MENU_NEXTSTATE_SIZE; i++)
    {
        /* Found current state in list and matching input */
        if((menu_nextstate[i].state == current_state) && (menu_nextstate[i].input == input))
        {
            /* Update next state */
            current_state = menu_nextstate[i].nextstate;
            
            /* Reset input since we just "used" it up for state switch */
            input = KEY_NONE;
            break;
        }
    }

    /* Execute function */
    for(k = 0; k < MENU_STATE_SIZE; k++)
    {
        if((menu_state[k].state == current_state) && (menu_state[k].p_func))
        {
            menu_state[k].p_func(input);
            break;
        }
    }
}

void menu_level_1(uint8_t input)
{
    uint16_t index = 0;
    
    /* Clear display */
    memset(lcd_buf, 0xFF, sizeof(lcd_buf));

    for(index = 0; index < MENU_STATE_SIZE; index++)
    {
        /* Highlight current selection by printing it with inverted color */
        if(menu_state[index].state == current_state && menu_state[index].p_text)
        {
            print_lcd_line(menu_state[index].p_text,
            strlen(menu_state[index].p_text), index, 0, 1);
        }
        /* Print menu item */
        else if(menu_state[index].p_text)
        {
            print_lcd_line(menu_state[index].p_text,
            strlen(menu_state[index].p_text), index, 0, 0);
        }
    }    

    send_framebuffer();
}

void menu_set_alti_state(uint8_t input)
{
    /* Clear LCD */
    current_state = ST_ENTRY1;
    global_state = DEV_STATE_DISP_ALTI;
}

void menu_show_status(uint8_t input)
{
    char buf[MAX_CHAR_NUM_LCD + 1]; /* (+ null terminator) */
    uint8_t l = 0;

    /* Clear display */
    memset(lcd_buf, 0xFF, sizeof(lcd_buf));

    /* Show battery voltage in mv, "????" if invalid */
    if(battery_voltage_valid)
    {
        l = snprintf(buf, MAX_CHAR_NUM_LCD + 1, "BATT V:%li", battery_voltage_v);
    }
    else
    {
        l = snprintf(buf, MAX_CHAR_NUM_LCD + 1, "BATT V:????");
    }
    print_lcd_line(buf, l, 0, 0, 0);

    /* Show temperature in C, "????" if invalid  */
    if(temperature_valid)
    {
        l = snprintf(buf, MAX_CHAR_NUM_LCD + 1, "TEMP C:%li", temperature_c);
    }
    else
    {
        l = snprintf(buf, MAX_CHAR_NUM_LCD + 1, "TEMP C:????");
    }
    print_lcd_line(buf, l, 1, 0, 0);

    /* Show altitude, both AGL and ASL, "????" if invalid */
    if(altitude_valid)
    {
        l = snprintf(buf, MAX_CHAR_NUM_LCD + 1, "AGL FT:%li", altitude_agl_ft);
        print_lcd_line(buf, l, 2, 0, 0);
        l = snprintf(buf, MAX_CHAR_NUM_LCD + 1, "ASL FT:%li", altitude_asl_ft);
        print_lcd_line(buf, l, 3, 0, 0);
    }
    else
    {
        l = snprintf(buf, MAX_CHAR_NUM_LCD + 1, "AGL FT:????");
        print_lcd_line(buf, l, 2, 0, 0);
        l = snprintf(buf, MAX_CHAR_NUM_LCD + 1, "ASL FT:????");
        print_lcd_line(buf, l, 3, 0, 0);
    }

    l = snprintf(buf, MAX_CHAR_NUM_LCD + 1, "DZ FT: %li", altitude_dz_ft);
    print_lcd_line(buf, l, 4, 0, 0);

    l = snprintf(buf, MAX_CHAR_NUM_LCD + 1, "UPTIME:%lu:%lu:%lu", 
                                            sched_cycles/72000, 
                                            (sched_cycles/1200) % 60, 
                                            (sched_cycles/20) % 60);
    print_lcd_line(buf, l, 5, 0, 0);

    send_framebuffer();
}


void menu_altitude_offset(uint8_t input)
{
    char str[MAX_CHAR_NUM_LCD + 1]; /* (+ null terminator) */
    uint8_t sz = 0;

    /* Set all LCD pixels to white */
    memset(lcd_buf, 0xFF, sizeof(lcd_buf));
    
    if(input == BUT_UP)
    {
        altitude_offset += 10;
    }
    else if(input == BUT_DOWN)
    {
        altitude_offset -= 10;   
    }
    else if(input == KEY_CENTRE)
    {
        current_state = ST_ENTRY4;
    }

    sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, "OFFSET: %li", altitude_offset);
    print_lcd_line(str, sz, 0, 0, 0);

    /* Send framebuffer to LCD */
    send_framebuffer();
}

void menu_zero_altitude(uint8_t input)
{
    /* Set dz altitude to current asl altitude */
    altitude_dz_ft = altitude_asl_ft;

    /* Reset filters for dz altitude atmospheric auto-correction */
    delay_filter(altitude_dz_ft, 1);
    lowpass_filter(altitude_dz_ft, 1);
}

void menu_about(uint8_t input)
{
    char str[MAX_CHAR_NUM_LCD + 1]; /* (+ null terminator) */
    uint8_t sz = 0;

    /* Clear display */
    memset(lcd_buf, 0xFF, sizeof(lcd_buf));

    sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, "(C) IVAN BARANOV");
    print_lcd_line(str, sz, 0, 0, 0);

    sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, "VERSION: %u", FW_VERSION);
    print_lcd_line(str, sz, 1, 0, 0);

    sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, "BUILD DATE/TIME:");
    print_lcd_line(str, sz, 3, 0, 0);

    sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, "%s", __DATE__);
    print_lcd_line(str, sz, 4, 0, 0);

    sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, "%s", __TIME__);
    print_lcd_line(str, sz, 5, 0, 0);

    send_framebuffer();
}

void menu_auto_zero(uint8_t input)
{
    static uint8_t selection = 0;
    char str[MAX_CHAR_NUM_LCD + 1]; /* Max LCD character number + null */
    uint8_t sz = 0;
    
    if(settings[SETTINGS_OFF_AUTO_ZERO] == 0)
    {
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, "CURRENT: DISABLED");
    }
    else if(settings[SETTINGS_OFF_AUTO_ZERO] == 1)
    {
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, "CURRENT: ENABLED");
    }

    /* Clear display */
    memset(lcd_buf, 0xFF, sizeof(lcd_buf));
    print_lcd_line(str, sz, 0, 0, 0);
    
    /* Currently cursor on ENABLE */
    if(selection == 0)
    {
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " DISABLE         ");
        print_lcd_line(str, sz, 6, 0, 0);
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " ENABLE          ");
        print_lcd_line(str, sz, 7, 0, 1);

        /* User selected 2 significant figure */
        if(input == KEY_CENTRE)
        {
            settings[SETTINGS_OFF_AUTO_ZERO] = 1u;
            current_state = ST_ENTRY8;
        }
        else if(input == KEY_DOWN || input == KEY_UP)
        {
            selection = 1;
        }
    }
    else if(selection == 1) /* Currently cursor on DISABLE */
    {
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " DISABLE         ");
        print_lcd_line(str, sz, 6, 0, 1);
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " ENABLE          ");
        print_lcd_line(str, sz, 7, 0, 0);

        /* User selected 2 significant figure */
        if(input == KEY_CENTRE)
        {
            settings[SETTINGS_OFF_AUTO_ZERO] = 0u;
            current_state = ST_ENTRY8;
        }
        else if(input == KEY_DOWN || input == KEY_UP)
        {
            selection = 0;
        }
    }

    send_framebuffer();
}

void menu_sig_figs(uint8_t input)
{
    static uint8_t selection = 0;
    char str[MAX_CHAR_NUM_LCD + 1]; /* Max LCD character number + null */
    uint8_t sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, "CURRENT: %u", 
                                                   settings[SETTINGS_OFF_SIG_FIGS]);

    /* Clear display */
    memset(lcd_buf, 0xFF, sizeof(lcd_buf));
    print_lcd_line(str, sz, 0, 0, 0);
    
    /* Currently cursor on SET 3 SIG FIGS */
    if(selection == 0)
    {
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " SET 2 SIG FIGS  ");
        print_lcd_line(str, sz, 6, 0, 0);
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " SET 3 SIG FIGS  ");
        print_lcd_line(str, sz, 7, 0, 1);

        /* User selected 2 significant figure */
        if(input == KEY_CENTRE)
        {
            settings[SETTINGS_OFF_SIG_FIGS] = 3u;
            current_state = ST_ENTRY7;
        }
        else if(input == KEY_DOWN || input == KEY_UP)
        {
            selection = 1;
        }
    }
    else if(selection == 1) /* Currently cursor on SET 2 SIG FIGS */
    {
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " SET 2 SIG FIGS  ");
        print_lcd_line(str, sz, 6, 0, 1);
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " SET 3 SIG FIGS  ");
        print_lcd_line(str, sz, 7, 0, 0);

        /* User selected 2 significant figure */
        if(input == KEY_CENTRE)
        {
            settings[SETTINGS_OFF_SIG_FIGS] = 2u;
            current_state = ST_ENTRY7;
        }
        else if(input == KEY_DOWN || input == KEY_UP)
        {
            selection = 0;
        }
    }

    send_framebuffer();
}

void menu_shutdown(uint8_t input)
{
    static uint8_t selection = 0;
    char str[MAX_CHAR_NUM_LCD + 1]; /* Max LCD character number + null */
    uint8_t sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, "SHUTDOWN ALTI?");

    /* Clear display */
    memset(lcd_buf, 0xFF, sizeof(lcd_buf));
    print_lcd_line(str, sz, 0, 0, 0);
    
    if(selection == 0)
    {
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " YES             ");
        print_lcd_line(str, sz, 6, 0, 0);
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " NO              ");
        print_lcd_line(str, sz, 7, 0, 1);

        if(input == KEY_CENTRE)
        {
            current_state = ST_ENTRY1;           
        }
        else if(input == KEY_DOWN || input == KEY_UP)
        {
            selection = 1;
        }
    }
    else if(selection == 1)
    {
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " YES             ");
        print_lcd_line(str, sz, 6, 0, 1);
        sz = snprintf(str, MAX_CHAR_NUM_LCD + 1, " NO              ");
        print_lcd_line(str, sz, 7, 0, 0);

        if(input == KEY_CENTRE)
        {
            shutdown_device();
        }
        else if(input == KEY_DOWN || input == KEY_UP)
        {
            selection = 0;
        }
    }

    send_framebuffer();
}
