#include <p24FJ64GA002.h>
#include <stdio.h>
#include "common.h"

/* Filter constants for delay and lowpass filters */
#define DELAY_BUF_SIZE 4*120    /* 120s delay buffer at 4HZ sample rate */
#define LOWPASS_FACTOR 0.001f   /* time constant = 250s at 4HZ sample rate */

uint16_t device_faulted = 0;

/* Reserve 1024 pc-unit (1536 bytes, of which we use 1024 bytes) flash block for */
/* non-volatile user data. It is not possible to use the variable directly */
uint8_t nvm_block[1024] __attribute__((space(prog), aligned(0x400), section(".nvm"))) = {0x00};

/* Defn of settings array, all zeros */
uint16_t settings[SETTINGS_OFF_CHKSUM + 1] = {0};

/* Default values used on initial boot and factory reset */
const uint16_t settings_default[SETTINGS_OFF_CHKSUM + 1] = {0x0026, 0x0003, 0x0000, 0x0000, 0x0000};

/* List of fault occurences */
uint16_t fault_occurences[MAX_FAULT_TYPES];

/* Timeout flag */
static int16_t timeout_flag = 0;

int16_t log_assert_violation(char *file, int32_t line, char *assert)
{
    (void)file;
    (void)line;
    (void)assert;

    device_faulted = 1;

    return 1;
}

int16_t log_assert_violation_code(char *file, int32_t line, char *assert, uint16_t code)
{
    (void)file;
    (void)line;
    (void)assert;
    
    device_faulted = 1;

    if(code > MAX_FAULT_TYPES)
    {
        return 1;
    }
    
    /* Saturate count at 9, since we're only displaying 1 digits per count */
    if(fault_occurences[code] < 9)
    {
        fault_occurences[code]++;
    }

    return 1;
}

/* Timeout setup function, the timer period is passed here using predefined values */
void setup_timeout(uint16_t timeout_val)
{
    T2CONbits.TON = 1;      /* Turn timer ON */
    T2CONbits.TCKPS = 1;    /* 500KHz tick (4MHz Fcy/8) */
    PR2 = timeout_val;      /* Preload with #defined value */
    TMR2 = 0;               /* Reset count */
    IFS0bits.T2IF = 0;      /* Reset interrupt flag */
    timeout_flag = 0;       /* Reset timeout flag */
}

/* This function handles multiple reads of timeout status. We can't use the T2IF */
/* flag directly to detect timeout since it's reset after the first read. */
inline int16_t is_timeout(void)
{
    
    if(IFS0bits.T2IF)
    {
        /* Set flag, such that next time this is called it still lets caller know of a timeout, unless another setup was performed */
        timeout_flag = 1;
    }

    return timeout_flag;
}

/* This saves settings to flash and then powers down device */
void shutdown_device(void)
{
    uint8_t i = 0;

    /* Stop task scheduling */
    IEC0bits.T1IE = 0;

    /* Erase our NVM flash block*/
    erase_nvm();

    /* Write setttings array to flash (except last word which is CRC) */
    for(i = 0; i < sizeof(settings)/sizeof(uint16_t) - 1; i++)
    {
        write_word(settings[i], i);
    }

    /* Write CRC, use only the data bytes minus the CRC bytes */
    write_word(crc16((uint8_t*)settings, sizeof(settings) - sizeof(uint16_t)), SETTINGS_OFF_CHKSUM);

    /* Wait for user to un-press button */
    while(!BUT_CENTRE)
    {
        /* Clear WDT such that we don't reset prematurely */
        asm("CLRWDT");
    }

    /* Un-latch power */
    PWR_EN = 0;
    
    /* Wait so nothing else gets executed/corrupted while power is falling */
    while(1);
}

/* Erases whole non-volatile data section that is used for settings and such */
void erase_nvm(void)
{
    uint16_t addr = (uint16_t)nvm_block;
    uint16_t offset; 
    TBLPAG = 0;
    offset = addr & 0xFFFF;

    NVMCON = 0x4042;
    __builtin_tblwtl(offset, 0x0000);
    __builtin_write_NVM();
}

/* Write 16-bit word to specific offset */
void write_word(uint16_t word, uint16_t index)
{
    uint16_t addr = (uint16_t)nvm_block + index*2;
    uint16_t offset;

    /* Have a maximum offset number */
    if(c_assert_code(index < sizeof(nvm_block)/sizeof(uint16_t), FLT_NVM_WRITE_OOR))
    {
        return;
    }

    NVMCON = 0x4003;
    TBLPAG = 0;
    offset = addr & 0xFFFF;

    __builtin_tblwtl(offset, word);
    __builtin_write_NVM();
}

/* Read 16-bit word from specific offset */
uint16_t read_word(uint16_t index)
{
    uint16_t addr = (uint16_t)nvm_block + index*2;
    uint16_t offset;

    if(c_assert_code(index < sizeof(nvm_block)/sizeof(uint16_t), FLT_NVM_READ_OOR))
    {
        return 0;
    }

    TBLPAG = 0;
    offset = addr & 0xFFFF;
    return __builtin_tblrdl(offset);
}

/* Calculate CRC16-CCITT */
uint16_t crc16(uint8_t data[], uint16_t length)
{
    uint16_t crc = 0x1D0F;

    /* Only support up to 1024 bytes, check pointer */
    if(c_assert(length <= 1024) || 
       c_assert(data))
    {
        return 0;
    }

    while (length--)
    {
        uint8_t x = crc >> 8 ^ *data++;
        x ^= x >> 4;
        crc = (crc << 8) ^ ((uint16_t)x << 12) ^ ((uint16_t)x << 5) ^ ((uint16_t)x);
    }

    return crc;
}

int32_t delay_filter(int32_t input, uint8_t reset)
{
    static int32_t delay_buffer[DELAY_BUF_SIZE];
    static int32_t index = 0;
    static int32_t initial = 1;

    /* Initialize or reset all buffer values to initial value*/
    if (initial || reset)
    {
        int32_t i = 0;
        for (i = 0; i < DELAY_BUF_SIZE; i++)
        {
            delay_buffer[i] = input;
        }

        initial = 0;
    }

    /* Overwrite the oldest sample in buffer to current sample*/
    delay_buffer[index] = input;

    /* Update index to the "new" oldest sample*/
    index++;
    if (index >= DELAY_BUF_SIZE)
    {
        index = 0;
    }

    return delay_buffer[index];
}

int32_t lowpass_filter(int32_t input, uint8_t reset)
{
    static float output = 0.0f;
    static int32_t initial = 1;

    /* Initialize filtered value*/
    if(initial || reset)
    {
        output = (float)input;

        initial = 0;
    }

    /* Slow but simple, fixed point has too many loss of precision gotchas */
    output = LOWPASS_FACTOR*(float)input + (1.0f - LOWPASS_FACTOR)*output;

    return (int32_t)output;
}

