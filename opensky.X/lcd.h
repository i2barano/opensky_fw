#ifndef LCD_HEADER
#define LCD_HEADER

/* LCD commands*/
#define LCD_SET_COL_ADDR_LSB    (uint8_t)0x00 /* OR lower 4 bits */
#define LCD_SET_COL_ADDR_MSB    (uint8_t)0x10 /* OR lower 4 bits */
#define LCD_SET_PWR_CTRL        (uint8_t)0x28 /* OR 3 lower bits: bit0->Booster, bit1->Regulator, bit3->Follower */
#define LCD_SET_SCRL_LINE       (uint8_t)0x40 /* OR lower 6 bits */
#define LCD_SET_PAGE_ADDR       (uint8_t)0xB0 /* OR lower 4 bits, valid page addresses are [0-7] */
#define LCD_CONF_VLCD_RES       (uint8_t)0x20 /* OR lower 3 bits, valid ratios are [0-7]] */
#define LCD_SET_CNTRST_16b      (uint16_t)0x8100 /* OR lower 6 bits, valid range [0-63] */
#define LCD_SET_ALL_PIX_ON      (uint8_t)0xA4 /* OR lower 1 bit, 0-> show SRAM content, 1->all drivers ON */
#define LCD_SET_INV_DISP        (uint8_t)0xA6 /* OR lower 1 bit, 0-> show normal SRAM, 1-> show inverse SRAM */
#define LCD_DISP_EN             (uint8_t)0xAE /* OR lower 1 bit, 0-> disable display (sleep), 1->enable display */
#define LCD_SEG_DIR             (uint8_t)0xA0 /* OR lower 1 bit, 0-> normal SEG [0-131], 1-> mirror SEG [131-0] */
#define LCD_COM_DIR             (uint8_t)0xC0 /* OR bit 3 (1 << 3), 0-> normal COM [0-63], 1-> reverse COM [63-0] */
#define LCD_RST_CMD             (uint8_t)0xE2 /* Reset LCD controller */
#define LCD_SET_BIAS            (uint8_t)0xA2 /* OR lower bit, 0-> 1/9 bias, 1-> 1/7 bias */
#define LCD_ADV_CONTROL0_16b    (uint16_t)0xFA10 /* OR bit 7 (1 << 7), 0-> -0.05%/1C, 1-> -0.11%/1C, OR bit 1/bit 2=column/page wraparound */

/* Width/height of DOGS102 display */
#define LCD_W 102u
#define LCD_H 64u

#define LCD_PAGES 8

/* Maximum number of horizontal 5x5 characters spaced out to 8x8 squares */
#define MAX_CHAR_NUM_LCD 17u

/* Horizontal offsets for altitude digits */
#define DIG_OFFSET_0    0u   /* Digit 1 in 1.23 or 12.3 */
#define DIG_OFFSET_1_H  32u  /* Digit 2 in 1.23 */
#define DIG_OFFSET_1_L  42u  /* Digit 2 in 12.3 */
#define DIG_OFFSET_2    74u  /* Digit 3 in 1.23 or 12.3 */

int16_t print_lcd_line(char *str, uint8_t length, uint8_t line, uint8_t offset, uint8_t invert);
int16_t send_framebuffer(void);
int16_t print_alti_digit(int16_t digit, int16_t value, int16_t minus);

extern uint8_t lcd_buf[LCD_W*LCD_H/8];

#endif /* LCD_HEADER*/

