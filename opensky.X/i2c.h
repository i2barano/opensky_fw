#ifndef I2C_H
#define I2C_H

int16_t i2c_start(uint8_t address);
int16_t i2c_rep_start(uint8_t address);
int16_t i2c_stop(void);
int16_t i2c_send(uint8_t data);
int16_t i2c_recv_nack(uint8_t *value);
int16_t i2c_recv_ack(uint8_t *value);
void i2c_reset(void);

#endif /* I2C_H */
