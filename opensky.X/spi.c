#include <p24FJ64GA002.h>
#include <stdint.h>
#include "spi.h"
#include "common.h"

int16_t send_spi_8b_array(const uint8_t *data, uint16_t size)
{
    uint16_t i = 0;
    
    setup_timeout(TIMEOUT_10MS);

    for(i = 0; i < size && !is_timeout(); i++)
    {
        /* Wait for previous word to be transfered from buffer to shift register */
        while(SPI1STATbits.SPITBF && !is_timeout());

        /* Flush bogus received byte */
        (void)SPI1BUF;

        if(!SPI1STATbits.SPITBF)
        {
            SPI1BUF = data[i];
        }
        else
        {
            break; /* Timeout on SPITBF */
        }
    }

    if(c_assert_code(!is_timeout(), FLT_TIMEOUT_SPI_ARRAY))
    {
        return 1;
    }
 
    return 0;
}

uint8_t send_recv_spi_8b(uint8_t data)
{
    SPI1CON1bits.MODE16 = 0; /* 8 bit mode */

    setup_timeout(TIMEOUT_1MS);

    /* Wait for previous word to be transfered from buffer to shift register */
    while(SPI1STATbits.SPITBF && !is_timeout());

    /* Clear SPIRBF and set SPIRBE */
    (void)SPI1BUF;

    if(!SPI1STATbits.SPITBF)
    {
        SPI1BUF = data;
    }
    else
    {
        return 1; /* Timeout on SPITBF */
    }

    /* Wait for receive word */
    while(!SPI1STATbits.SPIRBF && !is_timeout());

    c_assert_code(!is_timeout(), FLT_TIMEOUT_SPI_8B);

    return (uint8_t)SPI1BUF;
}

uint16_t send_recv_spi_16b(uint16_t data)
{
    setup_timeout(TIMEOUT_1MS);

    SPI1CON1bits.MODE16 = 1; /* 16 bit mode */

    /* Wait for previous word to be transfered from buffer to shift register */
    while(SPI1STATbits.SPITBF && !is_timeout());

    /* Clear SPIRBF and set SPIRBE */
    (void)SPI1BUF;

    if(!SPI1STATbits.SPITBF)
    {   
        SPI1BUF = data;
    }
    else
    {   
        return 1; /* Timeout on SPITBF */
    }

    /* Wait for receive word */
    while(!SPI1STATbits.SPIRBF && !is_timeout());

    c_assert_code(!is_timeout(), FLT_TIMEOUT_SPI_16B);

    return (uint16_t)SPI1BUF;
}
