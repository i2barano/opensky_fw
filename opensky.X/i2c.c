#include <p24FJ64GA002.h>
#include <stdint.h>
#include "common.h"
#include "i2c.h"

int16_t i2c_start(uint8_t address)
{
    /* Ensure previously generated condition was not a start */
    if(c_assert_code(!I2C2STATbits.S, FLT_I2C_PREV_NOT_STOP))
    {
        return 1;
    }

    /* Assert start condition */
    I2C2CONbits.SEN = 1;
    
    setup_timeout(TIMEOUT_100US);

    /* Wait for start condidition to assert */
    while(!I2C2STATbits.S && !is_timeout());
    
    /* Catch timeout condition */
    if(c_assert_code(!is_timeout(), FLT_I2C_S_TIMEOUT))
    {
        return 1;
    }

    /* Send address */
    return i2c_send(address);
}

int16_t i2c_stop(void)
{
    /* Ensure master logic is disabled */
    /* Ensure previously generated condition was not a stop */
    if( c_assert_code((I2C2CON & 0x1F) == 0, FLT_I2C_MSTR_EN1) ||
        c_assert_code(!I2C2STATbits.P, FLT_I2C_PREV_NOT_START))
    {
        return 1;
    }

    I2C2CONbits.PEN = 1;

    setup_timeout(TIMEOUT_100US);

    /* Wait for stop condition to assert */
    while(I2C2STATbits.S && !is_timeout());

    if(c_assert_code(!is_timeout(), FLT_I2C_P_TIMEOUT))
    {
        return 1;
    }

    return 0;
}

int16_t i2c_send(uint8_t data)
{
    /* Ensure start condition asserted */
    if(c_assert(I2C2STATbits.S))
    {
        return 1;
    }

    setup_timeout(TIMEOUT_1MS);

    /* Wait for transmit buffer to become empty */
    while(I2C2STATbits.TBF && !is_timeout());
    if(c_assert_code(!is_timeout(), FLT_I2C_TBF_TIMEOUT))
    {
        return 1;
    }

    I2C2TRN = data;

    while(I2C2STATbits.TRSTAT && !is_timeout())
    if(c_assert_code(!is_timeout(), FLT_I2C_TRSTAT_TIMEOUT))
    {
        return 1;
    }

    /* Return 1 if NAK, 0 if ACK */
    return I2C2STATbits.ACKSTAT;
}

int16_t i2c_recv_nack(uint8_t *value)
{
    /* Assert that lower 5 bits of I2C2CON are 0 and that pointer is not null */
    if( c_assert_code((I2C2CON & 0x1F) == 0, FLT_I2C_MSTR_EN2) ||
        c_assert(value))
    {
        return 1;
    }

    /* Enable receive mode */
    I2C2CONbits.RCEN = 1;

    /* Takes ~80uS to send/receive one byte at 100KHz, we set the timeout to 1mS to accomodate that */
    setup_timeout(TIMEOUT_1MS);

    while(!I2C2STATbits.RBF && !is_timeout());

    /* Catch timeout */
    if(c_assert_code(!is_timeout(), FLT_I2C_RBF_TIMEOUT))
    {
        return 1;
    }

    /* Start NACK sequence (ACKDT=1) */
    I2C2CONbits.ACKDT = 1;
    I2C2CONbits.ACKEN = 1;

    /* Wait for NACK to complete */
    while(I2C2CONbits.ACKEN && !is_timeout());

    /* Catch timeout */
    if(c_assert(!is_timeout()))
    {
        return 1;
    }

    /* Set output variable to received byte */
    *value = I2C2RCV;

    return 0;
}

int16_t i2c_recv_ack(uint8_t *value)
{
    /* Assert that lower 5 bits of I2C2CON are 0 and that pointer is not null */
    if( c_assert_code((I2C2CON & 0x1F) == 0, FLT_I2C_MSTR_EN2) ||
        c_assert(value))
    {
        return 1;
    }

    /* Enable receive mode */
    I2C2CONbits.RCEN = 1;

    /* Takes ~80uS to send/receive one byte at 100KHz, we set the timeout to 1mS to accomodate that */
    setup_timeout(TIMEOUT_1MS);

    while(!I2C2STATbits.RBF && !is_timeout());

    /* Catch timeout */
    if(c_assert_code(!is_timeout(), FLT_I2C_RBF_TIMEOUT))
    {
        return 1;
    }

    /* Start ACK sequence (ACKDT=0) */
    I2C2CONbits.ACKDT = 0;
    I2C2CONbits.ACKEN = 1;

    /* Wait for ACK to complete */
    while(I2C2CONbits.ACKEN && !is_timeout());

    /* Catch timeout */
    if(c_assert(!is_timeout()))
    {
        return 1;
    }

    /* Set output variable to received byte */
    *value = I2C2RCV;

    return 0;
}

int16_t i2c_rep_start(uint8_t address)
{
    /* Assert that lower 5 bits of I2C2CON are 0 */
    if(c_assert_code((I2C2CON & 0x1F) == 0, FLT_I2C_MSTR_EN3))
    {
        return 1;
    }   
    
    /* Initiate repeated start condition, hardware clears bit */
    I2C2CONbits.RSEN = 1;

    setup_timeout(TIMEOUT_100US);

    /* Wait for repeated start condition to occur */
    while(I2C2CONbits.RSEN && !is_timeout());

    /* Catch timeout */
    if(c_assert_code(!is_timeout(), FLT_I2C_RSEN_TIMEOUT))
    {
        return 1;
    }

    /* Send address */
    return i2c_send(address);
}

void i2c_reset(void)
{
    /* Do a false assert to log i2c reset invocation due to fault */
    (void)c_assert_code(0, FLT_I2C_RESET_INVOKED);

    /* Reset lower 5 bits of I2C2CON */
    I2C2CON &= ~(0x1F);

    setup_timeout(TIMEOUT_100US);

    /* Initiate stop bit */
    I2C2CONbits.PEN = 1;

    /* Wait for hardware clear of stop bit, may timeout if last state was already a stop bit */
    while (I2C2CONbits.PEN && !is_timeout());

    /* Clear some control and status bits */
    I2C2CONbits.RCEN = 0;
    I2C2STATbits.IWCOL = 0;
    I2C2STATbits.BCL = 0;

    /* Catch timeout */
    c_assert_code(!is_timeout(), FLT_I2C_RESET_TIMEOUT);
}
