# README #

This is the firmware/software part of the OpenSky open-source skydiving altimeter project.
It consists of a single MPLAB X XC16 project and some simple code generation binaries.

* Hardware: PIC24FJ MCU + MPL3115A2 Altimeter + DOGS102 LCD
* Software: Co-operative scheduler with a few tasks for reading altimeter module, displaying, and handling user input