#!/bin/sh

cflow --symbol __leaf__:wrapper --symbol __extension__:wrapper --symbol __const__:wrapper --symbol __nothrow__:wrapper --symbol __restrict:wrapper -D__PIC24FJ64GA002__ -I /opt/microchip/xc16/v1.26/support/PIC24F/h/ -I ./opensky.X/ ./opensky.X/*.c
